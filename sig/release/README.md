# Release SIG

The Release SIG aims at development and release management, including plan, requirement, issue, risk and so on.

## SIG Mission and Scope

- requirement and feature management

- release plan, development plan

- development process and risk managemnet

- release scope management, release note

## Repository
- cckylin-seeds
- all

## SIG members
### Owner
zhangchao
### Maintainers
handsome_feng
maozhou
xiewei

## Email
release@cckylin.com
